﻿using FluentValidation;
using UserDetails.Commands;

namespace UserDetails.Validators
{
    class GetAuthorizationTokenCommandValidator : AbstractValidator<GetAuthorizationTokenCommand>
    {
        public GetAuthorizationTokenCommandValidator() : base()
        {
            RuleFor(command => command.Email).NotNull();
            RuleFor(command => command.Password).NotNull();
        }
    }
}
