﻿using FluentValidation;
using UserDetails.Commands;

namespace UserDetails.Validators
{
    public class ResetPasswordCommandValidator : AbstractValidator<ResetPasswordCommand>
    {
        public ResetPasswordCommandValidator() : base()
        {
            RuleFor(command => command.Id).NotNull();
            RuleFor(command => command.Password).NotNull();
            RuleFor(command => command.RepeatPassword).NotNull();

            RuleFor(command => command.Password).Equal(command => command.RepeatPassword);
        }
    }
}