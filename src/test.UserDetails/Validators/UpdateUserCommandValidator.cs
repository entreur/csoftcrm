﻿using FluentValidation;
using UserDetails.Commands;

namespace UserDetails.Validators
{
    public class UpdateUserCommandValidator : AbstractValidator<UpdateUserCommand>
    {
        public UpdateUserCommandValidator() : base()
        {
            RuleFor(command => command.Id).NotNull();
            RuleFor(command => command.Email).NotNull();
            RuleFor(command => command.FirstName).NotNull();
            RuleFor(command => command.LastName).NotNull();
            RuleFor(command => command.RoleId).NotNull();
        }
    }
}
