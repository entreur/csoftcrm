﻿using FluentValidation;
using UserDetails.Commands;

namespace UserDetails.Validators
{
    public class RegisterUserCommandValidator : AbstractValidator<RegisterUserCommand>
    {
        public RegisterUserCommandValidator() : base()
        {
            RuleFor(command => command.Password).NotNull().MinimumLength(8);
            RuleFor(command => command.Email).NotNull();
            RuleFor(command => command.FirstName).NotNull();
            RuleFor(command => command.LastName).NotNull();
            RuleFor(command => command.RoleId).NotNull();
        }
    }
}