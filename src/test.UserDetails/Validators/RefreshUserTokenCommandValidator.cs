﻿using FluentValidation;
using UserDetails.Commands;

namespace UserDetails.Validators
{
    public class RefreshUserTokenCommandValidator : AbstractValidator<RefreshUserTokenCommand>
    {
        public RefreshUserTokenCommandValidator() : base()
        {
            RuleFor(command => command.RefreshToken).NotNull();
        }
    }
}
