﻿using UserDetails.Commands.Models;
using MediatR;

namespace UserDetails.Commands
{
    public class GetAuthorizationTokenCommand : IRequest<JwtTokenDTO>
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
