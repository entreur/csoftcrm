﻿using UserDetails.Commands.Models;
using MediatR;

namespace UserDetails.Commands
{
    public class RefreshUserTokenCommand : IRequest<JwtTokenDTO>
    {
        public string RefreshToken { get; set; }
    }
}
