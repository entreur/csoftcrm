﻿using FluentValidation;
using UserDetails.Commands.OTP;

namespace UserDetails.Commands.Models
{
    class ForgotPasswordCommandValidator : AbstractValidator<SendOTPEmail>
    {
        public ForgotPasswordCommandValidator() : base()
        {
            RuleFor(command => command.Email).NotNull();
        }
    }
}
