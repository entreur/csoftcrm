﻿using MediatR;

namespace UserDetails.Commands
{
    public class DeleteUserCommand : IRequest<bool>
    {
        public List<int> Id { get; set; }
    }
}
