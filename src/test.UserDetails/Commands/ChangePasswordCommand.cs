﻿using UserDetails.Commands.Models;
using MediatR;

namespace UserDetails.Commands
{
    public class ChangePasswordCommand : IRequest<bool>
    {
        public string OldPassword { get; set; }

        public string NewPassword { get; set; }

        public string NewPasswordConfirm { get; set; }
    }
}
