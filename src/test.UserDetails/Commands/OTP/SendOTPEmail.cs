﻿using MediatR;

namespace UserDetails.Commands.OTP
{
    /// <summary>
    /// Forgot Password - Step One
    /// </summary>
    public class SendOTPEmail : IRequest<bool>
    {
        public string Email { get; set; }
    }
}
