﻿using MediatR;

namespace UserDetails.Commands.OTP
{
    /// <summary>
    /// Forgot Password - Step Two
    /// </summary>
    public class OtpConfirmationCommand : IRequest<bool>
    {
        public int OTP { get; set; }
    }
}
