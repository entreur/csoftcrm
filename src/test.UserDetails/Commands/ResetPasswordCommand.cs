﻿using MediatR;

namespace UserDetails.Commands
{
    public class ResetPasswordCommand : IRequest<bool>
    {
        public int Id { get; set; }
        public string Password { get; set; }
        public string RepeatPassword { get; set; }
    }
}
