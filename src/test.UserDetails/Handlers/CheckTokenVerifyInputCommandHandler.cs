﻿using Identity.Queries;
using Infrastructure;
using Infrastructure.Commands;
using Infrastructure.Idempotency;
using UserDetails.Commands.Models;
using MediatR;
using Microsoft.Extensions.Options;
using System.Security.Authentication;
using UserDetails.Commands;

namespace UserDetails.Handlers
{
    public class CheckTokenVerifyInputCommandHandler : IRequestHandler<CheckTokenVerifyInputCommand, bool>
    {
        private readonly IUserQueries _userQueries;
        private readonly CRMSettings _settings;


        public CheckTokenVerifyInputCommandHandler(IUserQueries userQueries, IOptions<CRMSettings> settigns)
        {
            _userQueries = userQueries ?? throw new ArgumentNullException(nameof(userQueries));
            _settings = settigns.Value ?? throw new ArgumentNullException(nameof(settigns));
        }

        public async Task<bool> Handle(CheckTokenVerifyInputCommand request, CancellationToken cancellationToken)
        {
            var user = await _userQueries.FindByEmailAsync(request.Email.ToLower())
                ?? throw new AuthenticationException("Invalid credentials.");

            var userId = user.Id.ToString(); 

            var validateTokenUserId = TokenManager.ValidateToken(_settings, request.JwtToken)
                ?? throw new AuthenticationException("Token is null");

            if (!userId.Equals(validateTokenUserId))
            {
                throw new AuthenticationException("Token is invalid for this user");
            }

            return true;
        }

        public class CheckTokenInputCommandHandler : IdentifiedCommandHandler<CheckTokenVerifyInputCommand, bool>
        {
            public CheckTokenInputCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
            {
            }

            protected override bool CreateResultForDuplicateRequest()
            {
                return true;
            }
        }
    }
}
