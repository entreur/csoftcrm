﻿using MediatR;
using Domain.AggregatesModel.EmployeeAggregate;
using Domain.AggregatesModel.RoleAggregate;
using Identity.Auth;
using Identity.Queries;
using Infrastructure.Commands;
using Infrastructure.Idempotency;
using UserDetails.Commands;

namespace UserDetails.Handlers
{
    public class ActiveUserCommandHandler : IRequestHandler<ActiveUserCommand, bool>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserQueries _userQueries;
        private readonly IUserManager _userManager;

        public ActiveUserCommandHandler(IUserQueries userQueries, IUserManager userManager, IUserRepository userRepository)
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _userQueries = userQueries ?? throw new ArgumentNullException(nameof(userQueries));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public async Task<bool> Handle(ActiveUserCommand request, CancellationToken cancellationToken)
        {
            User? user = await _userQueries.FindAsync(request.Id);
            User? self = await _userManager.GetCurrentUser(); //Trying to check priviliages of the user doing the actions.

            if (self.RoleId == RoleParameter.SuperAdmin.Id && self.Id == request.Id)
            {
                // SuperAdmin cannot deactivate itself
                throw new UnauthorizedAccessException("SUPERADMIN cannot be deactivated, not even by itself!");
            }

            if (self.RoleId == RoleParameter.Admin.Id && request.Id == RoleParameter.SuperAdmin.Id)
            {
                // Admin cannot deactivate SuperAdmin
                throw new UnauthorizedAccessException("Admin cannot deactivate SuperAdmin!");
            }

            if (self.RoleId == RoleParameter.Admin.Id && self.Id == request.Id)
            {
                // Admin cannot deactivate itself
                throw new UnauthorizedAccessException("Admin cannot deactivate itself!");
            }

            user.SetActivated(request.IsActive);
            _userRepository.UpdateAsync(user);
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return true;
        }
    }

    public class ActiveUserIdentifiedCommandHandler : IdentifiedCommandHandler<ActiveUserCommand, bool>
    {
        public ActiveUserIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}