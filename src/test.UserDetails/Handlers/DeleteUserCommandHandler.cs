﻿using Domain.AggregatesModel.EmployeeAggregate;
using Identity.Auth;
using Identity.Queries;
using Infrastructure.Commands;
using Infrastructure.Idempotency;
using MediatR;
using UserDetails.Commands;

namespace UserDetails.Handlers
{
    public class DeleteUserCommandHandler : IRequestHandler<DeleteUserCommand, bool>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserQueries _userQueries;
        private readonly IUserManager _userManager;

        public DeleteUserCommandHandler(IUserQueries userQueries, IUserRepository userRepository, IUserManager userManager)
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _userQueries = userQueries ?? throw new ArgumentNullException(nameof(userQueries));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userQueries));
        }

        public async Task<bool> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            var self = await _userManager.GetCurrentUser();
            foreach (int id in request.Id)
            {
                User? user = await _userQueries.FindAsync(id);
                if (self.RoleId >= user.RoleId)
                    throw new UnauthorizedAccessException("You cannot delete administrators or superadminitstrators");
                user.IsDeleted = true;
                _userRepository.UpdateAsync(user);
            }
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
    }

    public class DeleteUserIdentifiedCommandHandler : IdentifiedCommandHandler<DeleteUserCommand, bool>
    {
        public DeleteUserIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}