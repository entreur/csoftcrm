﻿using Domain.AggregatesModel.EmployeeAggregate;
using Identity.Auth;
using Identity.Queries;
using SharedKernel.Infrastructure;
using UserDetails.Commands.Models;
using MediatR;
using System.Security.Authentication;
using UserDetails;
using UserDetails.Commands;
using Domain.Exceptions;

namespace UserDetails.Handlers
{
    public class GetAuthorizationTokenCommandHandler : IRequestHandler<GetAuthorizationTokenCommand, JwtTokenDTO>
    {
        private readonly IUserManager _userManager;
        private readonly IUserQueries _userQueries;
        private readonly IUserRepository _userRepository;

        public GetAuthorizationTokenCommandHandler(IUserManager userManager, IUserQueries userQueries, IUserRepository userRepository)
        {
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _userQueries = userQueries ?? throw new ArgumentNullException(nameof(userQueries));
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
        }

        public async Task<JwtTokenDTO> Handle(GetAuthorizationTokenCommand request, CancellationToken cancellationToken)
        {
            User? user = await _userQueries.FindByEmailAsync(request.Email) 
                ?? throw new DomainException($"No user registered via {request.Email}");

            if (user.IsDeleted == true || user.IsActive == false)
                throw new AuthenticationException("Account has been deactivated.");

            if (user == null || user.PasswordHash != PasswordHasher.HashPassword(request.Password))
                throw new AuthenticationException("Invalid credentials.");


            (string token, DateTime expiresAt) = _userManager.GenerateJwtToken(user);

            string? refreshTokenValue;
            if (user.RefreshToken != null)
            {
                var splitToken = user.RefreshToken.Split("_");

                if (Convert.ToDateTime(splitToken[2]) < DateTime.Now)
                {
                    var randomNumber = GenerateRandomNumberExtension.GenerateRandomNumber();
                    var refreshToken = $"{randomNumber}_{user.Id}_{DateTime.UtcNow.AddDays(30)}";
                    refreshTokenValue = refreshToken;
                    user.UpdateRefreshToken(refreshToken);
                }
                else
                {
                    refreshTokenValue = user.RefreshToken;
                }
            }
            else
            {
                var randomNumber = GenerateRandomNumberExtension.GenerateRandomNumber();
                var refreshToken = $"{randomNumber}_{user.Id}_{DateTime.UtcNow.AddDays(30)}";
                refreshTokenValue = refreshToken;
                user.UpdateRefreshToken(refreshToken);
            }

            _userRepository.UpdateAsync(user);
            await _userRepository.UnitOfWork.SaveChangesAsync(cancellationToken);

            return new JwtTokenDTO
            {
                Token = token,
                RefreshToken = refreshTokenValue,
                ExpiresAt = expiresAt
            };
        }
    }
}
