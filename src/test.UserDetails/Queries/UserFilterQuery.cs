﻿using Identity.ViewModels;
using Infrastructure.Constants;
using MediatR;

namespace UserDetails.Queries
{
	public class UserFilterQuery : IRequest<AllUsersResponse>
	{
		public string Firstname { get; set; } = string.Empty;
		public string Lastname { get; set; } = string.Empty;
		public List<int> TeamIds { get; set; }
		public List<int> ProjectIds { get; set; }
		public LoadMoreDto? LoadMore { get; set; }
	}
}
