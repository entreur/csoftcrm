﻿using Domain.AggregatesModel.EmployeeAggregate;
using System.Collections.Generic;
using System.Security.Claims;

namespace Infrastructure.Identity
{
    public interface IClaimsManager
    {
        int GetCurrentUserId();

        string GetCurrentUserName();

        IEnumerable<Claim> GetUserClaims(User user);

        Claim GetUserClaim(string claimType);
    }
}
