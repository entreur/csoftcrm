﻿using Domain.AggregatesModel.ProjectAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityConfigurations.ProjectUserEntityConfiguration
{
    public class ProjectUserEntityConfiguration : IEntityTypeConfiguration<UserProject>
    {
        public void Configure(EntityTypeBuilder<UserProject> configuration)
        {
            configuration.ToTable("user_project");
            configuration.HasKey(x => x.Id);

            configuration
                .HasIndex(up => new { up.UserId, up.ProjectId })
                .IsUnique();

            configuration
                .HasOne(up => up.User)
                .WithMany(u => u.UserProjects)
                .HasForeignKey(up => up.UserId);

            configuration
                .HasOne(up => up.Project)
                .WithMany(p => p.UserProjects)
                .HasForeignKey(up => up.ProjectId);
        }
    }
}
