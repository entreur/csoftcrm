﻿using Domain.AggregatesModel.DailyReportAggregate;
using Microsoft.EntityFrameworkCore;
using Report;
using Infrastructure.Database;
using SharedKernel.Infrastructure;

namespace Infrastructure.Repositories
{
    public class DailyReportRepository : Repository<DailyReport>, IReportRepository
    {
        public sealed override DbContext Context { get; protected set; }

        public DailyReportRepository(AppDBContext context)
        {
            Context = context;
        }
    }
}
