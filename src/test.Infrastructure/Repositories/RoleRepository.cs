﻿using Domain.AggregatesModel.RoleAggregate;
using Infrastructure.Database;
using SharedKernel.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        public sealed override DbContext Context { get; protected set; }

        public RoleRepository(AppDBContext context)
        {
            Context = context;
        }
    }
}
