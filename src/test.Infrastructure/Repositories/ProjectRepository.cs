﻿using Domain.AggregatesModel.ProjectAggregate;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Database;
using SharedKernel.Infrastructure;

namespace Infrastructure.Repositories
{
    public class ProjectRepository : Repository<ProjectEntity>, IProjectRepository
    {
        public sealed override DbContext Context { get; protected set; }

        public ProjectRepository(AppDBContext context)
        {
            Context = context;
        }
    }
}