﻿using Domain.AggregatesModel.DailyReportAggregate;
using Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using Report;
using SharedKernel.Infrastructure;

namespace Infrastructure.Repositories
{
    public class OTPRepository : Repository<DailyReport>, IReportRepository
    {
        public sealed override DbContext Context { get; protected set; }

        public OTPRepository(AppDBContext context)
        {
            Context = context;
        }
    }
}
