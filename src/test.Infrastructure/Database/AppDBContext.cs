﻿using Domain.AggregatesModel.ProjectAggregate;
using Domain.AggregatesModel.TeamAggregate;
using Infrastructure.EntityConfigurations.ProjectEntityConfiguration;
using Infrastructure.EntityConfigurations.TeamEntityConfiguration;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Domain.AggregatesModel.RoleAggregate;
using Domain.AggregatesModel.EmployeeAggregate;
using Infrastructure.EntityConfigurations.AuditEntityConfiguration;
using Infrastructure.EntityConfigurations.IdentityEntityConfiguration;
using Infrastructure.EntityConfigurations.RoleEntityConfiguration;
using SharedKernel.Domain.Seedwork;
using SharedKernel.Infrastructure;
using Domain.AggregatesModel.DailyReportAggregate;
using Infrastructure.EntityConfigurations.ProjectUserEntityConfiguration;
using Infrastructure.EntityConfigurations.DailyReportEntityConfiguration;
namespace Infrastructure.Database
{
    public sealed class AppDBContext : DbContext, IUnitOfWork
    {
        public const string IDENTITY_SCHEMA = "Identity";
        public const string DEFAULT_SCHEMA = "dbo";

        private readonly IMediator _mediator;

        public AppDBContext(DbContextOptions options, IMediator mediator) : base(options)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        #region Db Sets
        public DbSet<User> Users { get; private set; }
        public DbSet<Role> Roles { get; private set; }
        public DbSet<ProjectEntity> Projects { get; private set; }
        public DbSet<UserProject> UserProjects { get; private set; }
        public DbSet<DailyReport> DailyReports { get; private set; }
        public DbSet<TeamEntity> Teams { get; private set; }
        #endregion

        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            await _mediator.DispatchDomainEventsAsync(this);
            await SaveChangesAsync(true, cancellationToken);

            return true;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ClientRequestEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RoleEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TeamEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectUserEntityConfiguration());
            modelBuilder.ApplyConfiguration(new DailyReportEntityConfiguration());

          
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.ClientSetNull;
            }
        }
    }
}
