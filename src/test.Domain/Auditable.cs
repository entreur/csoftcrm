﻿using System.ComponentModel.DataAnnotations.Schema;
using Domain.AggregatesModel.EmployeeAggregate;
using Domain.Exceptions;
using SharedKernel.Domain.Seedwork;

namespace Domain
{
    public class Auditable<TUser> : Entity where TUser : User
    {
        [ForeignKey("created_by")]
        public int created_by_id { get; protected set; }

        public DateTime record_date { get; protected set; }

        public TUser created_by { get; protected set; }

        public void SetAuditFields(int createdById)
        {
            if (created_by_id != 0 && created_by_id != createdById)
            {
                throw new DomainException("CreatedBy already set");
            }

            created_by_id = createdById;
            record_date = DateTime.UtcNow;
        }
    }
}
