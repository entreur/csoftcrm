﻿using Domain.AggregatesModel.EmployeeAggregate;
using SharedKernel.Domain.Seedwork;

namespace Domain.AggregatesModel.TeamAggregate
{
    public class TeamEntity : Entity, IAggregateRoot
    {
        public string Name { get; private set; }

        private readonly List<User> _employees;
        public IReadOnlyCollection<User> _teamMembers => _employees;

        public TeamEntity()
        {
            _employees = new List<User>();
        }

        public void SetDetails(string name)
        {
            Name = name;
        }
    }
}