﻿using SharedKernel.Domain.Seedwork;

namespace Domain.AggregatesModel.RoleAggregate
{
    public class RoleParameter : Enumeration
    {
        public static RoleParameter SuperAdmin = new(1, RoleName.SuperAdmin);
        public static RoleParameter Admin = new(2, RoleName.Admin);
        public static RoleParameter Employee = new(3, RoleName.Employee);
        public static RoleParameter Head = new(4, RoleName.Head);

        public RoleParameter(int id, string name) : base(id, name)
        {

        }

        public RoleParameter()
        {

        }
    }
}
