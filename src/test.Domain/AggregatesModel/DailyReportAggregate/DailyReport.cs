﻿using Domain.AggregatesModel.EmployeeAggregate;
using Domain.AggregatesModel.ProjectAggregate;
using SharedKernel.Domain.Seedwork;

namespace Domain.AggregatesModel.DailyReportAggregate
{
    public class DailyReport : Editable<User>, IAggregateRoot 
    {
        public string Summary { get; private set; }
        public ProjectEntity Project { get; set; }
        public int ProjectId { get; private set; }  

        public void SetDetails(string summary, int projectId)
        {
            Summary = summary;          
            ProjectId = projectId;
        }
    }
}