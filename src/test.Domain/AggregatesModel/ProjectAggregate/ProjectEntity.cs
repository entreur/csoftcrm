﻿using Domain.AggregatesModel.DailyReportAggregate;
using SharedKernel.Domain.Seedwork;

namespace Domain.AggregatesModel.ProjectAggregate
{
    public class ProjectEntity : Entity, IAggregateRoot
    {
        public string Title { get; private set; }
        public DateTime CreatedDate { get; private set; }

        private readonly List<UserProject> _userProjects;
        public IReadOnlyCollection<UserProject> UserProjects => _userProjects;

        private readonly List<DailyReport> _reports;
        public IReadOnlyCollection<DailyReport> UserReports => _reports;

        public void SetDetails(string title, DateTime createdDate)
        {
            Title = title;  
            CreatedDate = createdDate;
        }

        public ProjectEntity()
        {
            _userProjects = new List<UserProject>();
            _reports = new List<DailyReport>();
        }
    }
}
