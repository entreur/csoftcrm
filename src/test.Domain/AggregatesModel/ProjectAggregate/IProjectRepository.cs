﻿using SharedKernel.Domain.Seedwork;

namespace Domain.AggregatesModel.ProjectAggregate
{
    public interface IProjectRepository : IRepository<ProjectEntity>
    {
    }
}
