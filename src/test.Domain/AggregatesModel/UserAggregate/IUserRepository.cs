﻿using SharedKernel.Domain.Seedwork;

namespace Domain.AggregatesModel.EmployeeAggregate
{
    public interface IUserRepository : IRepository<User>
    {
    }
}