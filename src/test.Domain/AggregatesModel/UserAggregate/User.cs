﻿using Domain.AggregatesModel.DailyReportAggregate;
using Domain.AggregatesModel.ProjectAggregate;
using Domain.AggregatesModel.TeamAggregate;
using SharedKernel.Domain.Seedwork;
using Domain.AggregatesModel.RoleAggregate;
using Domain.Exceptions;

namespace Domain.AggregatesModel.EmployeeAggregate
{
    public class User : Entity, IAggregateRoot, ISoftDeleteable
    {
        public string Firstname { get; private set; }
        public string Lastname { get; private set; }
        public string Email { get; private set; }
        public string PasswordHash { get; private set; }
        public bool IsActive { get; private set; }
        public string? RefreshToken { get; private set; }

        public int? OTP { get; private set; }
        public DateTime? OTPExpirationDate { get; private set; }

        public int RoleId { get; private set; }
        public int? TeamId { get; private set; }
        public TeamEntity? Team { get; private set; }
        public Role Role { get; private set; }

        private readonly List<DailyReport> _reports;
        public IReadOnlyCollection<DailyReport> Reports => _reports;
        private readonly List<UserProject> _userProject;
        public IReadOnlyCollection<UserProject> UserProjects => _userProject;

        // This plays the role of void SoftDelete() Method.
        public bool IsDeleted
        {
            get
            {
                return _isDeleted;
            }
            set
            {
                _isDeleted = value;
            }
        }

        private bool _isDeleted {get; set;}
        //Move this to SetDetails
        public User(string name, string lastname, string email, string passwordHash) : this()
        {
            Firstname = name;
            Lastname = lastname;
            Email = email;
            PasswordHash = passwordHash;
            IsActive = true;
        }

        public User()
        {
            _userProject = new List<UserProject>();
            _reports = new List<DailyReport>();
        }

        public void SetPasswordHash(string oldPasswordHash, string newPasswordHash)
        {
            if (PasswordHash != oldPasswordHash)
            {
                throw new DomainException("Invalid old password");
            }

            if (PasswordHash != newPasswordHash)
            {
                PasswordHash = newPasswordHash;
            }
        }

        public void ResetPassword(string newPasswordHash)
        {
            PasswordHash = newPasswordHash;
            OTP = null;
            OTPExpirationDate = null;
        }

        public void UpdateRefreshToken(string token)
        {
            RefreshToken = token;
        }

        //This thing is only used in update....
        public void SetDetails(string email, string name, string lastname)
        {
            Firstname = name;
            Lastname = lastname;
            Email = email;
        }

        public void SetRole(int roleId)
        {
            RoleId = roleId;
        }

        public void SetTeam(int? teamId)
        {
            TeamId = teamId;
        }

        //Name of the method doesn't do it justice.
        public void SetActivated(bool isActive)
        {
            IsActive = isActive;
        }

        public void SetOTP(int otp) 
        {
            OTP = otp;
            OTPExpirationDate = DateTime.UtcNow.AddMinutes(5);
        }
    }
}