﻿using AutoMapper;
using Domain.AggregatesModel.TeamAggregate;
using Microsoft.EntityFrameworkCore;
using Team.ViewModels;
using TeamModules.Queries;
using Domain.Exceptions;
using Infrastructure.Database;

namespace Team.Queries
{
    public class TeamQueries : ITeamQuery
    {
        private readonly AppDBContext _context;
        //private readonly IMapper _mapper;

        public TeamQueries(AppDBContext context/*, IMapper mapper*/)
        {
            _context = context;
            //_mapper = mapper;
        }

        public async Task<List<TeamEntity>> GetAllAsync() //Was TeamVM
        {
            /*var data = */ return await _context
                .Teams
                .ToListAsync();
           //return _mapper.Map<List<TeamVM>>(data);
        }

        public async Task<TeamEntity> GetTeamByIdAsync(int id) //Was TeamVM
        {
            /*var data =*/ return await _context
                .Teams
                .Include(t => t._teamMembers)
                .FirstOrDefaultAsync(t => t.Id == id) 
                ?? throw new DomainException("Entity not found.");
            //return _mapper.Map<TeamVM>(data);
        }
    }
}
