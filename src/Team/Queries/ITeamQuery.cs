﻿using Domain.AggregatesModel.TeamAggregate;
using Team.ViewModels;
using SharedKernel.Infrastructure.Queries;

namespace TeamModules.Queries
{
    public interface ITeamQuery : IQuery
    {
        Task<TeamEntity> GetTeamByIdAsync(int id);//was TeamVM
        Task<List<TeamEntity>> GetAllAsync();
    }
}