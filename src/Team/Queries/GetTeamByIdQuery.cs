﻿using MediatR;
using Team.ViewModels;

namespace Team.Queries
{
    public class GetTeamByIdQuery : IRequest<TeamVM>
    {
        public int Id { get; set; }
    }
}
