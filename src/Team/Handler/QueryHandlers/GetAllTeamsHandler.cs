﻿using AutoMapper;
using Domain.AggregatesModel.TeamAggregate;
using MediatR;
using Team.Queries;
using Team.ViewModels;
using TeamModules.Queries;

namespace Team.Handler.QueryHandlers
{
    public class GetAllTeamsHandler : IRequestHandler<GetAllTeamsQuery, List<TeamVM>>
    {
        private readonly ITeamQuery _teamQueries;
        private readonly IMapper _mapper;
        public GetAllTeamsHandler(ITeamQuery teamQueries, IMapper mapper)
        {
            _teamQueries = teamQueries;
            _mapper = mapper;
        }

        public async Task<List<TeamVM>> Handle(GetAllTeamsQuery request, CancellationToken cancellationToken)
        {
            var data = await _teamQueries.GetAllAsync();
            return _mapper.Map<List<TeamVM>>(data);
        }
    }
}
