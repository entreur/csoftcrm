﻿using Domain.AggregatesModel.TeamAggregate;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TeamModules.Commands;
using TeamModules.Queries;
using Domain.AggregatesModel.EmployeeAggregate;
using Domain.Exceptions;
using Infrastructure.Commands;
using Infrastructure.Database;
using Infrastructure.Idempotency;

namespace TeamModules.Handler
{
    public class UpdateTeamHandler : IRequestHandler<UpdateTeamCommand, bool>
    {
        public readonly ITeamRepository _repo;
        private readonly AppDBContext _context;

        public UpdateTeamHandler(ITeamRepository repo,AppDBContext context)
        {
            _repo = repo;
            _context = context;
        }

        public async Task<bool> Handle(UpdateTeamCommand request, CancellationToken cancellationToken)
        {
            TeamEntity? currentTeam = await _context
                .Teams
                //.Include(t => t._teamMembers)
                .FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken) 
                ?? throw new DomainException("Entity could not be found");

            currentTeam.SetDetails(request.Name);
            _repo.UpdateAsync(currentTeam);
            await _repo.UnitOfWork.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
    public class RegisterUpdateTeamCommandHandler : IdentifiedCommandHandler<UpdateTeamCommand, bool>
    {
        public RegisterUpdateTeamCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}