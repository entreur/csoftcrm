﻿namespace Team.ViewModels
{
    /// <summary>
    /// Limited version of TeamDTO for GetAll and GetByIdUsers.
    /// </summary>
    public class TeamResponse
    {
        public string Name { get; set; }
    }
}
