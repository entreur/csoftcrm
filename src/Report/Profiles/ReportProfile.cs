﻿using AutoMapper;
using Domain.AggregatesModel.DailyReportAggregate;
using Domain.AggregatesModel.EmployeeAggregate;
using Domain.AggregatesModel.ProjectAggregate;
using Report.Response;

namespace Report.Profiles
{
    public class ReportProfile : Profile
    {
        public ReportProfile()
        {
            CreateMap<DailyReport, DailyReportResponse>();
            CreateMap<User, DailyReportUserResponse>()
                .ForMember(dest => dest.Fullname, opt => opt.MapFrom(src => $"{src.Firstname} {src.Lastname}"));
            CreateMap<ProjectEntity, DailyReportProjectResponse>();
        }
    }
}