﻿using Report.DTO;

namespace Report.Response
{
    public class AllReportsResponse
    {
        public IEnumerable<DailyReportResponse> Data { get; set; }
        public int TotalCount { get; set; }
    }
}
