﻿namespace Report.Response
{
    public class DailyReportResponse
    {
        public string Summary { get; set; }
        public DailyReportProjectResponse Project { get; set; }
        public DailyReportUserResponse? created_by { get; set; }
        public DateTime? record_date { get; set; }
        public DateTime? last_update_date { get; set; }
    }
}