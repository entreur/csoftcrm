﻿using OfficeOpenXml;
using Report.Response;

namespace Report
{
    public class ExportToExcelService
    {

        public static byte[] ExportToExcel(List<DailyReportResponse> report)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using var memoryStream = new MemoryStream();
            using (var package = new ExcelPackage(memoryStream))
            {
                var worksheet = package.Workbook.Worksheets.Add("Sheet1");
                int row = 2;
                GenerateColumnNames(worksheet);
                foreach (var item in report)
                {
                    worksheet.Cells[row, 1].Value = item.Project.Id;
                    worksheet.Cells[row, 2].Value = item.Project.Title;
                    worksheet.Cells[row, 3].Value = item.Summary;
                    worksheet.Cells[row, 4].Value = item.created_by.Fullname;
                    worksheet.Cells[row, 5].Value = item.record_date.ToString();
                    row++;
                }
                package.Save();
            }
            return memoryStream.ToArray();
        }

        private static void GenerateColumnNames(ExcelWorksheet worksheet)
        {
            worksheet.Cells[1, 1].Value = "Project Id";
            worksheet.Cells[1, 2].Value = "Project Title";
            worksheet.Cells[1, 3].Value = "Summary";
            worksheet.Cells[1, 4].Value = "Fullname";
            worksheet.Cells[1, 5].Value = "Created Date";
        }
    }
}