﻿using AutoMapper;
using Identity.Auth;
using Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using Report.DTO;
using Report.Response;
using System.Linq.Dynamic.Core;
namespace Report.Queries
{
    public class DailyReportQueries : IDailyReportQuery
    {
        private readonly AppDBContext _context;
        private readonly IMapper _mapper;
        private readonly IUserManager _user;

        public DailyReportQueries(AppDBContext context, IMapper mapper, IUserManager user)
        {
            _context = context;
            _mapper = mapper;
            _user = user;
        }

        public async Task<DailyReportResponse> GetDailyReportById(GetDailyReportByIdDTO request)
        {
            var report = await _context.DailyReports
                .Include(d => d.created_by)
                .Include(d => d.Project)
                .FirstOrDefaultAsync(x => x.Id == request.Id);

            return _mapper.Map<DailyReportResponse>(report);
        }

        public async Task<AllReportsResponse> GetDailyReportFilter(DailyReportFilterDTO request)
        {
            var entities = _context.DailyReports
                .Include(d => d.created_by)
                .Include(d => d.Project)
                .AsQueryable();

            entities = entities.Where(p =>
                (request.ProjectIds == null || !request.ProjectIds.Any() || request.ProjectIds.Contains(p.Project.Id)) &&
                (request.UserIds == null || !request.UserIds.Any() || request.UserIds.Contains(p.created_by_id)) &&
                //(!request.CreateDate.HasValue || p.record_date.Date == request.CreateDate.Value.Date) &&
                //(!request.UpdateDate.HasValue || (p.last_update_date != null && p.last_update_date.Value.Date == request.UpdateDate.Value.Date)) &&
                (!request.StartDate.HasValue || (p.record_date.Date >= request.StartDate.Value.Date)) &&
                (!request.EndDate.HasValue || (p.record_date.Date <= request.EndDate.Value.Date))
            ); 

            var count = (await entities.ToListAsync()).Count;
            if (request.LoadMore?.SortField != null)
            {
                if (request.LoadMore.OrderBy)
                {
                    entities = entities.OrderBy($"p=>p.{request.LoadMore.SortField}");
                }
                else
                {
                    entities = entities.OrderBy($"p=>p.{request.LoadMore.SortField} descending");
                }
            }

            if (request.LoadMore?.Skip != null && request.LoadMore?.Take != null)
            {
                entities = entities.Skip(request.LoadMore.Skip.Value).Take(request.LoadMore.Take.Value);
            }

            var mapped = _mapper.Map<IEnumerable<DailyReportResponse>>(entities);
            AllReportsResponse outputModel = new()
            {
                Data = mapped,
                TotalCount = count
            };
            return outputModel;
        }

        public async Task<AllReportsResponse> GetUserReportFilter(GetSelfDailyReportsDTO request)
        {
            var self = await _user.GetCurrentUser();
            var query = _context
                .DailyReports
                .Include(r => r.Project)
                .Where(p => p.created_by_id == self.Id)
                .AsQueryable();

            query = query.Where(p =>
                (request.StartDate == null || p.record_date.Date >= request.StartDate) &&
                (request.EndDate == null || p.record_date.Date <= request.EndDate));

            int count = await query.CountAsync();

            if (request.LoadMore.SortField != null)
            {
                if (!string.IsNullOrEmpty(request.LoadMore.SortField))
                {
                    if (request.LoadMore.OrderBy)
                    {
                        query = query.OrderBy($"p=>p.{request.LoadMore.SortField}");
                    }
                    else
                    {
                        query = query.OrderBy($"p=>p.{request.LoadMore.SortField} descending");
                    }
                }

                if (request.LoadMore.Skip != null && request.LoadMore.Take != null)
                {
                    query = query.Skip(request.LoadMore.Skip.Value).Take(request.LoadMore.Take.Value);
                }
            }
            var entities = await query.ToListAsync();
            var mapped = _mapper.Map<IEnumerable<DailyReportResponse>>(entities);

            AllReportsResponse outputModel = new()
            {
                Data = mapped,
                TotalCount = count
            };

            return outputModel;
        }
    }
}
