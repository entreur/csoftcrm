﻿using MediatR;
using Report.DTO;
using Report.Response;

namespace Report.Commands
{
    public class ExportExcelQuery : IRequest<FileResponse>
    {
        public DailyReportFilterDTO ReportFilter { get; set; }
    }
}
