﻿using MediatR;

namespace Report.Commands
{
    public class UpdateReportCommand : IRequest<bool>
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public string Summary { get; set; }
    }
}