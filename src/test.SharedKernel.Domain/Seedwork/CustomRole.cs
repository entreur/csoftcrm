﻿namespace SharedKernel.Domain.Seedwork
{
    public static class CustomRole
    {
        public const string Admin = "Admin";
        public const string SuperAdmin = "SuperAdmin";
        public const string Head = "Head";
        public const string Employee = "Employee";
    }
}