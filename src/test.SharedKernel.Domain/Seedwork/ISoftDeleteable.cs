﻿namespace SharedKernel.Domain.Seedwork
{
    public interface ISoftDeleteable
    {
        bool IsDeleted { get; set; }
    }
}
