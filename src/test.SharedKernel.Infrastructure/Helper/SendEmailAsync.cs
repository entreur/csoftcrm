﻿using Microsoft.Extensions.Options;
using System.Net;
using System.Net.Mail;
using Domain.Exceptions;

namespace SharedKernel.Infrastructure.Helper
{
    public class SendEmailAsync : ISendEmailAsync
    {
        private readonly EmailConfiguration _emailConfiguration;
        private readonly NotificationEmailConfiguration _notificationEmailConfiguration;

        public SendEmailAsync(IOptions<EmailConfiguration> emailConfiguration, IOptions<NotificationEmailConfiguration> notificationEmailConfiguration)
        {
            _emailConfiguration = emailConfiguration.Value;
            _notificationEmailConfiguration = notificationEmailConfiguration.Value;
        }

        public async Task SendEmailUserAsync(string email, string subject, string body, int? rowNumber)
        {
            if (email.Contains(" ") || email.Contains("<") || email.Contains(">"))
            {
                throw new DomainException($"{rowNumber}th row is invalid.");
            }

            SmtpClient client = new SmtpClient(_emailConfiguration.SmtpServer, 587);

            client.UseDefaultCredentials = false;
            client.TargetName = "test";
            client.Credentials = new NetworkCredential(_emailConfiguration.From, _emailConfiguration.Password);
            MailMessage message = new MailMessage(new MailAddress(_emailConfiguration.From, "test"), new MailAddress(email));
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            message.IsBodyHtml = true;
            message.Subject = subject;
            message.Body = body;
            message.From = new MailAddress(_emailConfiguration.From, "test");

            try
            {
                await client.SendMailAsync(message);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
        public async void SendEmailWithAttachmentsAsync(string email, string subject, string body, int? rowNumber, List<byte[]> attachments)
        {
            if (email.Contains(" ") || email.Contains("<") || email.Contains(">"))
            {
                throw new DomainException($"{rowNumber}th row is invalid.");
            }

            SmtpClient client = new SmtpClient(_emailConfiguration.SmtpServer, 587);

            client.UseDefaultCredentials = false;
            client.TargetName = "test";
            client.Credentials = new NetworkCredential(_emailConfiguration.From, _emailConfiguration.Password);
            MailMessage message = new MailMessage(new MailAddress(_emailConfiguration.From, "test"), new MailAddress(email));
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
         
            if (attachments != null)
            {
                byte[] fileBytes;
                foreach (var file in attachments)
                {
                    if (file.Length > 0)
                    {
                        MemoryStream stream = new MemoryStream(file);
                            var attachment = new Attachment(stream, "calendar.ics");

                            message.Attachments.Add(attachment);
                      

                    }
                }
            }
            message.IsBodyHtml = true;
            message.Subject = subject;
            message.Body = body;
            message.From = new MailAddress(_emailConfiguration.From, "test");
           
            try
            {
                await client.SendMailAsync(message);
                foreach (var attachment in message.Attachments)
                {
                    if (attachment != null)
                    {
                        attachment.Dispose();
                    }
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
        public async void SendNotificationEmailAsync(string email, string subject, string body, int? rowNumber)
        {
            if (email.Contains(" ") || email.Contains("<") || email.Contains(">"))
            {
                throw new DomainException($"{rowNumber}th row is invalid.");
            }

            SmtpClient client = new SmtpClient(_emailConfiguration.SmtpServer, 587);

            client.UseDefaultCredentials = false;
            client.TargetName = "test";
            client.Credentials = new NetworkCredential(_notificationEmailConfiguration.From, _notificationEmailConfiguration.Password);
            MailMessage message = new MailMessage(new MailAddress(_notificationEmailConfiguration.From, "test"), new MailAddress(email));
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            message.IsBodyHtml = true;
            message.Subject = subject;
            message.Body = body;
            message.From = new MailAddress(_notificationEmailConfiguration.From, "test");

            try
            {
                await client.SendMailAsync(message);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

    }

}
