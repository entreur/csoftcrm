﻿using Microsoft.AspNetCore.Mvc;
using CustomAuthorizeLogic;
using Identity.ViewModels;
using SharedKernel.Domain.Seedwork;
using Infrastructure.Constants;
using UserDetails.Queries;

namespace Presentation.Controllers.User
{
    public partial class UserController : ControllerBase
    {
        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin,CustomRole.Head)]
        [HttpGet]
        public async Task<AllUsersResponse> GetAllAsync(LoadMoreDto loadMore)
        {
            return await _userQueries.GetAllAsync(loadMore);
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        [HttpGet]
        [Route("/api/filter")]
        public async Task<AllUsersResponse> GetFilteredUser(UserFilterDTO request, LoadMoreDto dto)
        {
            return await _userQueries.GetFilteredUsers(request,dto);
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin, CustomRole.Head)]
        [HttpGet("{id}")]
        public async Task<UserResponse> GetByIdAsync(int id)
        {
            return await _userQueries.GetUserById(id);
        }
    }
}
