﻿using CustomAuthorizeLogic;
using Extensions;
using Identity.Queries;
using SharedKernel.Domain.Seedwork;
using UserDetails.Commands;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UserDetails.Commands.OTP;

namespace Presentation.Controllers.User
{
    [Route("api/user")]
    public partial class UserController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IUserQueries _userQueries;

        public UserController(IMediator mediator, IUserQueries userQueries)
        {
            _mediator = mediator ?? throw new ArgumentException(nameof(mediator));
            _userQueries = userQueries ?? throw new ArgumentNullException(nameof(userQueries));
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterUserCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<RegisterUserCommand, bool>(command, requestId);
            return NoContent();
        }

        [HttpPut("changePassword")]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<ChangePasswordCommand, bool>(command, requestId);
            return Ok();
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("sendotp")]
        public async Task<IActionResult> SendOTPCode([FromBody] SendOTPEmail command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<SendOTPEmail, bool>(command, requestId);
            return NoContent();
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("validateotp")]
        public async Task<IActionResult> ValidateOTP([FromQuery] OtpConfirmationCommand command)
        {
            await _mediator.Send(command);
            return NoContent();
        }

        [AllowAnonymous]
        [HttpPatch]
        [Route("forgotpassword")]
        public async Task<IActionResult> ForgotPassword([FromBody] ChangePasswordWithOTPCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<ChangePasswordWithOTPCommand, bool>(command, requestId);
            return NoContent();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] UpdateUserCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<UpdateUserCommand, bool>(command, requestId);
            return Ok();
        }

        [HttpPost("validate")]
        public async Task<IActionResult> ValidateToken([FromBody] CheckTokenVerifyInputCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<CheckTokenVerifyInputCommand, bool>(command, requestId);
            return Ok();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin,CustomRole.Admin)] 
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] DeleteUserCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<DeleteUserCommand, bool>(command, requestId);
            return NoContent();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPatch("changestatus")]
        public async Task<IActionResult> ChangeStatus([FromBody] ActiveUserCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<ActiveUserCommand, bool>(command, requestId);
            return NoContent();
        }
    }
}