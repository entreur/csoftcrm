﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Team.Commands;
using TeamModules.Commands;
using TeamModules.Queries;
using CustomAuthorizeLogic;
using Extensions;
using SharedKernel.Domain.Seedwork;

namespace Presentation.Controllers.Team
{
    [Route("api/[controller]")]
    [ApiController]
    public partial class TeamController : ControllerBase
    {
        private readonly IMediator _mediator;
        public TeamController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentException(nameof(mediator));           
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPost("create")]
        public async Task<IActionResult> CreateTeam([FromBody] CreateTeamCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<CreateTeamCommand, bool>(command, requestId);
            return NoContent();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpPut("update")]
        public async Task<IActionResult> UpdateTeam([FromBody] UpdateTeamCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<UpdateTeamCommand, bool>(command, requestId);
            return NoContent();
        }

        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        [HttpDelete("delete")]
        public async Task<IActionResult> DeleteTeam([FromBody] DeleteTeamCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<DeleteTeamCommand, bool>(command, requestId);
            return NoContent();
        }
    }
}