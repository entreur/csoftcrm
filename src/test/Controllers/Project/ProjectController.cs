﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Project.Commands;
using CustomAuthorizeLogic;
using Extensions;
using SharedKernel.Domain.Seedwork;
using UserDetails.Commands;
using Identity.Queries;

namespace Presentation.Controllers.Project
{
    [Route("api/[controller]")]
    [ApiController]
    public partial class ProjectController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IProjectQueries _query;

        public ProjectController(IMediator mediator, IProjectQueries queries)
        {
            _mediator = mediator;
            _query = queries;
        }

        [HttpPost]
        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        public async Task<IActionResult> CreateProject([FromBody] CreateProjectCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<CreateProjectCommand, bool>(command, requestId);
            return NoContent();
        }

        [HttpPut]
        [AuthorizeRoles(CustomRole.SuperAdmin, CustomRole.Admin)]
        public async Task<IActionResult> UpdateProject([FromBody] UpdateProjectCommand command, [FromHeader(Name = "x-requestid")] string requestId)
        {
            await _mediator.ExecuteIdentifiedCommand<UpdateProjectCommand, bool>(command, requestId);
            return NoContent();
        }
    }
}