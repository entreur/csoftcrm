﻿using Microsoft.AspNetCore.Mvc;
using Project.DTO;

namespace Presentation.Controllers.Project
{
    public partial class ProjectController : ControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> GetProject([FromQuery] ProjectFilterDTO request)
        {
            return Ok(await _query.GetAllAsync(request));
        }
        [HttpGet("id")]
        public async Task<IActionResult> GetProjectById([FromQuery] ProjectByIdDTO request)
        {
            return Ok(await _query.GetProjectById(request));
        }
    }
}