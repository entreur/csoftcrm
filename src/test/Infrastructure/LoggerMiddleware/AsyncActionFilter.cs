﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Infrastructure.LoggerMiddleware
{
    public class AsyncActionFilter : IAsyncActionFilter
    {

        private readonly ILogger<AsyncActionFilter> _logger;

        public AsyncActionFilter(ILogger<AsyncActionFilter> logger)
        {
            _logger = logger;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var req = $"|RequestId : { JsonConvert.SerializeObject(context.HttpContext.Connection.Id)} | Action : { JsonConvert.SerializeObject(context.ActionDescriptor.DisplayName)} | Body : {JsonConvert.SerializeObject(context.ActionArguments)}";
            _logger.LogInformation(req);

            var result = await next();

            if (result.Result is ObjectResult objectResult)
            {
                var ress = $"|RequestId : { JsonConvert.SerializeObject(context.HttpContext.Connection.Id)} | Action :  { JsonConvert.SerializeObject(context.ActionDescriptor.DisplayName)} | Body : {JsonConvert.SerializeObject(objectResult.Value)}";

                _logger.LogInformation(ress);
            }

            // execute any code after the action executes
        }
    }
}
