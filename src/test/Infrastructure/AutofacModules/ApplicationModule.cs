﻿using Autofac;
using AutoMapper;
using Domain.AggregatesModel.ProjectAggregate;
using Domain.AggregatesModel.TeamAggregate;
using Infrastructure.Repositories;
using Project;
using Report;
using TeamModules;
using Domain.AggregatesModel.EmployeeAggregate;
using Identity;
using Identity.Auth;
using Infrastructure.Idempotency;
using Infrastructure.Identity;
using UserDetails;

namespace Infrastructure.AutofacModules
{
    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            AutofacHelper.RegisterCqrsTypes<ApplicationModule>(builder);
            AutofacHelper.RegisterCqrsTypes<IdentityModule>(builder);
            AutofacHelper.RegisterCqrsTypes<UserModule>(builder);
            AutofacHelper.RegisterCqrsTypes<TeamModule>(builder);
            AutofacHelper.RegisterCqrsTypes<ProjectModule>(builder);
            AutofacHelper.RegisterCqrsTypes<ReportModule>(builder);
           
            // Repositories
            builder.RegisterType<ClaimsManager>()
                .As<IClaimsManager>()
                .InstancePerLifetimeScope();

            builder.RegisterType<RequestManager>()
                .As<IRequestManager>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserManager>()
                .As<IUserManager>()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserRepository>()
                .As<IUserRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<TeamRepository>()
                .As<ITeamRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ProjectRepository>()
                .As<IProjectRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<DailyReportRepository>()
               .As<IReportRepository>()
               .InstancePerLifetimeScope();

            //builder.RegisterType<OTPRepository>()
            //    .As<IOTPRepository>()
            //    .InstancePerLifetimeScope();

            // AutoMapper
            AutofacHelper.RegisterAutoMapperProfiles<ApplicationModule>(builder);
            AutofacHelper.RegisterAutoMapperProfiles<IdentityModule>(builder);
            AutofacHelper.RegisterAutoMapperProfiles<TeamModule>(builder);
            AutofacHelper.RegisterAutoMapperProfiles<UserModule>(builder);
            AutofacHelper.RegisterAutoMapperProfiles<ProjectModule>(builder);
            AutofacHelper.RegisterAutoMapperProfiles<ReportModule>(builder);
           

            builder.Register(ctx =>
            {
                var mapperConfiguration = new MapperConfiguration(cfg =>
                {
                    foreach (var profile in ctx.Resolve<IList<Profile>>()) cfg.AddProfile(profile);
                });
                return mapperConfiguration.CreateMapper();
            })
                .As<IMapper>()
                .SingleInstance();
        }
    }
}