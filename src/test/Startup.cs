using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Infrastructure;
using Infrastructure.AutofacModules;
using Infrastructure.Database;
using Infrastructure.ErrorHandling;
using Infrastructure.Filters;
using Infrastructure.Middlewares;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerUI;
using Newtonsoft.Json;
using Infrastructure.Constants;
using Infrastructure.Identity;
using SharedKernel.Infrastructure.Helper;

namespace test
{
    public class Startup
    {
        public IConfiguration Configuration { get; private set; }
        public IWebHostEnvironment WebEnvironment { get; }

        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            WebEnvironment = environment;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader());
            });

            services.AddControllers();
            services.AddControllers()
                .AddNewtonsoftJson(opt =>
                {
                    opt.SerializerSettings.Error = (_, args) =>
                    {
                        Console.WriteLine(args.ErrorContext.Error);
                    };

                    opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                });
            
            var settings = Configuration.Get<CRMSettings>();
            services.Configure<CRMSettings>(Configuration);
            services.Configure<SharedKernel.Infrastructure.EmailConfiguration>(Configuration.GetSection("EmailConfiguration"));
            services.Configure<SmsOptions>(Configuration.GetSection(SmsOptions.Sms));

            if (WebEnvironment.IsDevelopment())
            {
                services.Configure<DeyerlisenFileSettings>(Configuration.GetSection(DeyerlisenFileSettings.BasePathWindows));
            }
            else
            {
                services.Configure<DeyerlisenFileSettings>(Configuration.GetSection(DeyerlisenFileSettings.BasePathLinux));
            }

            // Add framework services.
            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(HttpGlobalExceptionFilter));
            })
                .AddControllersAsServices();

            services.AddEntityFrameworkSqlServer()
               .AddDbContext<AppDBContext>(options =>
               {
                   options.UseMySql(settings.DefaultConnection, ServerVersion.AutoDetect(settings.DefaultConnection),
                         sqlOptions =>
                         {
                             sqlOptions.MigrationsAssembly("Infrastructure.Migrations");
                             sqlOptions.EnableRetryOnFailure(10, TimeSpan.FromSeconds(3), new List<int>());
                         });
               }
               );

            ConfigureSwagger(services);
            ConfigureJwtAuthentication(services, settings);

            // Add application services.
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IClaimsManager, ClaimsManager>();
            services.AddSingleton<ISendEmailAsync, SendEmailAsync>();
            services.AddOptions();

            //configure Autofac
            var container = new ContainerBuilder();
            container.Populate(services);

            container.RegisterModule(new MediatorModule());
            container.RegisterModule(new ApplicationModule());

            return new AutofacServiceProvider(container.Build());
        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            var builder = new ConfigurationBuilder()
              .SetBasePath(env.ContentRootPath)
              .AddJsonFile("appsettings.json", true, true)
              .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
              .AddEnvironmentVariables();
            Configuration = builder.Build();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            app.UseMiddleware<AuthorizationErrorMiddleware>();

            app.UseSwagger()
              .UseSwaggerUI(c =>
              {
                  c.SwaggerEndpoint("/swagger/v1/swagger.json", "CSoftCRM");

                  c.DocumentTitle = "CSoftCRM";
                  c.DocExpansion(DocExpansion.List);
              });

            app.UseStaticFiles();

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors("CorsPolicy");

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }

        #region HelperMethods

        private void ConfigureSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "CSoftCRM",
                    Version = "v1",
                    Description = "CSoftCRM"
                });

                options.OperationFilter<AuthorizeCheckOperationFilter>();

                options.AddSecurityDefinition("bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: bearer {token}\"",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    In = ParameterLocation.Header,
                    BearerFormat = "JWT",
                    Scheme = "bearer"
                });
            });
        }

        private void ConfigureJwtAuthentication(IServiceCollection services, CRMSettings settings)
        {
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // => remove default claims
            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = settings.JwtIssuer,
                        ValidateAudience = true,
                        ValidAudience = settings.JwtIssuer,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(settings.JwtKey)),
                        ClockSkew = TimeSpan.FromSeconds(15),
                        ValidateLifetime = true
                    };
                });
        }
        #endregion
    }
}
