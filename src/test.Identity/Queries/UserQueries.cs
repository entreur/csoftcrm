﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Domain.AggregatesModel.EmployeeAggregate;
using Identity.ViewModels;
using Infrastructure.Constants;
using Infrastructure.Database;
using SharedKernel.Domain.Seedwork;
using System.Linq.Dynamic.Core;
using Identity.Auth;
using Microsoft.Extensions.DependencyInjection;

namespace Identity.Queries
{
	public class UserQueries : IUserQueries
    {
        private readonly AppDBContext _context;
        private readonly IServiceProvider _serviceProvider;
        private readonly IMapper _mapper;

        public UserQueries(AppDBContext context, IMapper mapper, IServiceProvider serviceProvider)
        {
            _context = context;
            _mapper = mapper;
            _serviceProvider = serviceProvider;
        }

        public async Task<User> FindAsync(int userId)
        {
            return await _context.Users.Include(p => p.Role)
                .FirstOrDefaultAsync(p => p.Id == userId);
        }

        public async Task<User> FindByNameAsync(string userName)
        {
            return await _context
                .Users
                .Include(p => p.Role)
                .FirstOrDefaultAsync(u => u.Firstname == userName);
        }

        public async Task<User> FindByEmailAsync(string email)
        {
            return await _context.Users
                .Include(u => u.Role)
                .FirstOrDefaultAsync(u => u.Email == email);
        }

        public async Task<UserProfileDto> GetUserProfileAsync(int userId)
        {
            var user = await _context.Users
                .Where(u => u.Id == userId)
                .Include(u => u.Role)
                .AsNoTracking()
                .SingleOrDefaultAsync() ?? null;

            return _mapper.Map<UserProfileDto>(user);
        }

        public async Task<User> GetUserEntityAsync(int? userId)
        {
            return await _context.Users
               .Where(u => u.Id == userId)
               .Include(u => u.Team)
               .Include(u => u.UserProjects)
               .AsNoTracking()
               .SingleOrDefaultAsync() ?? null;
        }

        public async Task<AllUsersResponse> GetAllAsync(LoadMoreDto loadMore)
        {
            var entities = _context.Users
                .Include(p => p.Role)
                .Include(p => p.Team)
                .Where(p => p.IsDeleted == false && p.Role.Name != CustomRole.SuperAdmin && p.Role.Name != CustomRole.Admin)
                .AsQueryable(); 

            var count = (await entities.ToListAsync()).Count;

            if (loadMore.SortField != null)
            {
                if (loadMore.OrderBy)
                {
                    entities = entities.OrderBy($"p=>p.{loadMore.SortField}");
                }
                else
                {
                    entities = entities.OrderBy($"p=>p.{loadMore.SortField} descending");
                }
            }
            //Issue.....
            if (loadMore.Skip != null && loadMore.Take != null)
            {
                entities = entities.Skip(loadMore.Skip.Value).Take(loadMore.Take.Value);
            }

            var userModel = _mapper.Map<IEnumerable<UserResponse>>(entities);
            AllUsersResponse outputModel = new()
            {
                Data = userModel,
                TotalCount = count
            };

            return outputModel;
        }

        public async Task<UserResponse> GetUserById(int id)
        {
            //Damn you front end team! This is your fault!
            //I only did what I was ordered T_T
            var entity = await _context
                .Users
                .Include(p => p.Role)
                .Include(p => p.Team)
                .Include(p => p.UserProjects)
                .ThenInclude(up => up.Project)
                .FirstOrDefaultAsync(p => p.Id == id && p.IsDeleted == false && p.Role.Name != CustomRole.SuperAdmin) 
                ?? throw new ArgumentNullException();

            return _mapper.Map<UserResponse>(entity);
        }

		public async Task<AllUsersResponse> GetFilteredUsers(UserFilterDTO request, LoadMoreDto dto)
		{
			var userManager = _serviceProvider.GetRequiredService<IUserManager>();
			var currentUser = await userManager.GetCurrentUser();
			var entities = _context.Users.Include(p => p.Role).Include(m => m.Team).Include(p => p.UserProjects).ThenInclude(up => up.Project)
		.Where(p =>
			(string.IsNullOrEmpty(request.Firstname) || p.Firstname.ToLower().Contains(request.Firstname.ToLower())) &&
			(string.IsNullOrEmpty(request.Lastname) || p.Lastname.ToLower().Contains(request.Lastname.ToLower())) &&
			!p.IsDeleted)
		.AsQueryable();
			if (currentUser.Role.Name == CustomRole.Admin)
			{
				entities = _context.Users
					.Where(x => x.Role.Name == CustomRole.Admin &&
				x.Id == currentUser.Id ||
				x.Role.Name == CustomRole.Employee)
					.Include(r => r.Role)
					.Include(t => t.Team)
					.Where(x => !x.IsDeleted);
			}

			entities = (request.ProjectIds != null && request.ProjectIds
				.Any()) ? entities
					.Where(p => p.UserProjects.Any(pu => request.ProjectIds
					.Contains(pu.ProjectId))) : entities;

			entities = (request.TeamIds != null && request.TeamIds.Any()) ? entities
				.Where(p => p.Team != null && request.TeamIds.Contains(p.Team.Id)) : entities;

			var count = (await entities.ToListAsync()).Count;

			if (dto.SortField != null)
			{
				entities = dto.OrderBy
					? entities.OrderBy($"p=>p.{dto.SortField}")
					: entities.OrderBy($"p=>p.{dto.SortField} descending");
			}

			if (dto.Skip != null && dto.Take != null)
			{
				entities = entities.Skip(dto.Skip.Value).Take(dto.Take.Value);
			}

            var userModel = _mapper.Map<IEnumerable<UserResponse>>(entities);

			var outputModel = new AllUsersResponse
			{
				Data = userModel,
				TotalCount = count
			};
			return outputModel;
		}
	}
}