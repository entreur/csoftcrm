﻿using Domain.AggregatesModel.EmployeeAggregate;
using Identity.ViewModels;
using Infrastructure.Constants;
using SharedKernel.Infrastructure.Queries;

namespace Identity.Queries
{
    public interface IUserQueries : IQuery
    {
        Task<User> FindByNameAsync(string userName);

        Task<User> FindByEmailAsync(string email);

        Task<User> FindAsync(int userId);

        Task<UserProfileDto> GetUserProfileAsync(int userId);

        Task<User> GetUserEntityAsync(int? userId);

        public Task<AllUsersResponse> GetAllAsync(LoadMoreDto loadMore);
        public Task<AllUsersResponse> GetFilteredUsers(UserFilterDTO request, LoadMoreDto dto);

        public Task<UserResponse> GetUserById(int id);
    }
}