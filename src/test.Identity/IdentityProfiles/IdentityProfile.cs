﻿using AutoMapper;
using Domain.AggregatesModel.RoleAggregate;
using Domain.AggregatesModel.EmployeeAggregate;
using Identity.ViewModels;
using Domain.AggregatesModel.TeamAggregate;
using Domain.AggregatesModel.ProjectAggregate;

namespace Identity.IdentityProfiles
{
    public class IdentityProfile : Profile
    {
        public IdentityProfile()
        {
            CreateMap<User, UserProfileDto>();
            CreateMap<User, UserResponse>();
            CreateMap<Role, RoleDto>();
            CreateMap<TeamEntity, TeamResponse>();
            CreateMap<UserProject, UserProjectResponse>();
            CreateMap<ProjectEntity, ProjectResponse>();
        }
    }
}
