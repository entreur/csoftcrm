﻿using Infrastructure.Constants;

namespace Identity.ViewModels
{
    public class UserFilterDTO
    {
        public string Firstname { get; set; } = string.Empty;
        public string Lastname { get; set; } = string.Empty;
        public List<int> TeamIds { get; set; }
        public List<int> ProjectIds { get; set; }
    }
}