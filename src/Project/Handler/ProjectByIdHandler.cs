﻿using AutoMapper;
using Domain.AggregatesModel.ProjectAggregate;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Project.Queries;
using Project.Response;
using Domain.Exceptions;
using Infrastructure.Database;

namespace Project.Handler
{
    public class ProjectByIdHandler : IRequestHandler<GetProjectFilterQuery, ProjectAllProjectsResponse>
    {
        private readonly AppDBContext _context;
        private readonly IMapper _mapper;

        public ProjectByIdHandler(AppDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ProjectAllProjectsResponse> Handle(GetProjectFilterQuery request, CancellationToken cancellationToken)
        {
        //    ProjectEntity? data = await _context.Projects
        //        .Include(p => p.UserProjects)
        //        .ThenInclude(up => up.User)
        //        .ThenInclude(u => u.Team)
        //        .FirstOrDefaultAsync(p => p.Id == request.Id, cancellationToken: cancellationToken)
        //        ?? throw new DomainException($"{request.Id} could not be found!");

        //    ProjectByIdResponse mapped = _mapper.Map<ProjectByIdResponse>(data);

        //    return mapped;
            throw new NotImplementedException();
        }
    }
}