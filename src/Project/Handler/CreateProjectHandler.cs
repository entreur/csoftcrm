﻿using Domain.AggregatesModel.ProjectAggregate;
using MediatR;
using Project.Commands;
using Infrastructure.Commands;
using Infrastructure.Database;
using Infrastructure.Idempotency;

namespace Project.Handler
{
    public class CreateProjectHandler : IRequestHandler<CreateProjectCommand, bool>
    {
        private readonly IProjectRepository _repo;
        private readonly AppDBContext _context;

        public CreateProjectHandler(IProjectRepository repo, AppDBContext context)
        {
            _repo = repo;
            _context = context;
        }

        public async Task<bool> Handle(CreateProjectCommand request, CancellationToken cancellationToken)
        {
            ProjectEntity newProject = new();
            newProject.SetDetails(request.Name,DateTime.UtcNow);
            await _repo.AddAsync(newProject);
            await _context.SaveChangesAsync(cancellationToken);
            //await _repo.UnitOfWork.SaveChangesAsync(cancellationToken); //Might be causing the issue?
            AddToUserProjectTable(newProject.Id,request.UserIds);
            await _context.SaveChangesAsync(cancellationToken);
            return true;
        }

        private async void AddToUserProjectTable(int projectId,int[] ids)
        {
            foreach (int id in ids)
            {
                UserProject userProject = new();
                userProject.SetDetails(id,projectId);
                await _context.AddAsync(userProject);
            }
        }
    }

    public class RegisterCreateProjectIdentifiedCommandHandler : IdentifiedCommandHandler<CreateProjectCommand, bool>
    {
        public RegisterCreateProjectIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator, requestManager)
        {
        }

        protected override bool CreateResultForDuplicateRequest()
        {
            return true;
        }
    }
}
