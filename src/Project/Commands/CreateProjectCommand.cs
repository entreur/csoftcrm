﻿using MediatR;

namespace Project.Commands
{
    public class CreateProjectCommand : IRequest<bool>
    {
        public string Name { get; set; }
        public int[] UserIds { get; set; }
    }
}