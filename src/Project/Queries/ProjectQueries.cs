﻿using AutoMapper;
using Identity.Queries;
using Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using Project.DTO;
using Project.Response;
using System.Linq.Dynamic.Core;


namespace Project.Queries
{
    public class ProjectQueries : IProjectQueries
    {
        private readonly AppDBContext _context;
        private readonly IMapper _mapper;

        public ProjectQueries(AppDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<AllProjectsResponse> GetAllAsync(ProjectFilterDTO request)
        {
            var entities = _context.Projects
            .Where(p =>
            request.Name == null || p.Title.ToLower()
            .Contains(request.Name.ToLower()))
            .AsQueryable();


            if (request.LoadMore?.SortField != null)
            {
                if (request.LoadMore.OrderBy)
                {
                    entities = entities.OrderBy("p => p.Title ascending");
                }
                else
                {
                    entities = entities.OrderBy("p => p.Title descending");
                }
            }

            int count = (await entities.ToListAsync()).Count;

            if (request.LoadMore?.Skip != null && request.LoadMore.Take != null)
            {
                entities = entities.Skip(request.LoadMore.Skip.Value).Take(request.LoadMore.Take.Value);
            }

            var userModel = _mapper.Map<IEnumerable<ProjectAllProjectsResponse>>(entities);
            AllProjectsResponse outputModel = new()
            {
                Data = userModel,
                TotalCount = count
            };

            return outputModel;
        }

        public async Task<GetProjectByIdResponse> GetProjectById(ProjectByIdDTO request)
        {
            var data = await _context.Projects
                .Include(p => p.UserProjects)
                .ThenInclude(up => up.User)
                .ThenInclude(u => u.Team)
                .FirstOrDefaultAsync(p => p.Id == request.Id);

            return _mapper.Map<GetProjectByIdResponse>(data);
        }
    }
}