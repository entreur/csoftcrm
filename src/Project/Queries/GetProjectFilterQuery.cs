﻿using MediatR;
using Project.Response;

namespace Project.Queries
{
    public class GetProjectFilterQuery : IRequest<ProjectAllProjectsResponse>
    {
        public int? Skip { get; set; }
        public int? Take { get; set; }
        public string Name { get; set; }
        public bool OrderBy { get; set; }
    }
}