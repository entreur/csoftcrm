﻿using Project.DTO;
using Project.Response;
using SharedKernel.Infrastructure.Queries;

namespace Identity.Queries
{
    public interface IProjectQueries : IQuery
    {
        public Task<AllProjectsResponse> GetAllAsync(ProjectFilterDTO request);
        public Task<GetProjectByIdResponse> GetProjectById(ProjectByIdDTO request);
    }
}