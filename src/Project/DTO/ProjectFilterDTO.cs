﻿using Infrastructure.Constants;

namespace Project.DTO
{
    public class ProjectFilterDTO
    {
        public string Name { get; set; } = string.Empty;
        public LoadMoreDto? LoadMore { get; set; }
    }
}