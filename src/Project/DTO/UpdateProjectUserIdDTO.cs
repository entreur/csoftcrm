﻿namespace Project.DTO
{
    public class UpdateProjectUserIdDTO
    {
        public int[]? AddUserIds { get; set; }
        public int[]? DeleteUserIds { get; set; }
    }
}