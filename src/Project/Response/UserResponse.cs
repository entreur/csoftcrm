﻿namespace Project.Response
{
    public class UserResponse
    {
        public string Fullname { get; private set; }
        public string Email { get; private set; }
        public TeamResponse Team { get; private set; }
    }
}
